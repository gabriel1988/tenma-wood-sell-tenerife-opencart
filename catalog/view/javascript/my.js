$(document).ready(function(){
    var count = parseFloat($('#price_first h2').html()).toFixed(2),
    stock = parseFloat($('#in_the_stock').html()),
    after = $('h2#count_after'),
    qty = $('#input-quantity');
    after.html("Total: "+count+"€");
    qty.keyup(function() {
    if(parseInt(qty.val()) >= stock) {
        if ($('html').attr('lang') == 'es') {
            alert('Cantidad no puede exceder cantidad en stock.');
        }
        else {
            alert('Quantity can not exceed quantity in the stock.');
        }
        qty.val(stock);
        $('#in_the_stock').css({'font-weight':'bold', 'color':'red', 'border-bottom':'solid 2px red', 'padding':'0px 30px', 'font-size':'20px'});
    }
    if (qty.val().length === 0) {
        after.html("Total: "+count+"€");
    }
    else {
        after.html("Total: "+(qty.val()*count).toFixed(2)+"€");
    }
    })
    /* if (qty.val() >= stock) {
        alert(shit);
        qty.val() = stock;
    }*/
});
