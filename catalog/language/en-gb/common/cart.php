<?php
// Text
$_['text_items']     = '<span class="item_number">%s </span><br/>Cart <br/><span class="item_price">%s</span>';
$_['text_empty']     = 'Your shopping cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Payment Profile';